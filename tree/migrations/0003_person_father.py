# Generated by Django 3.1.4 on 2020-12-22 11:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tree', '0002_auto_20201222_1134'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='father',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='father1', to='tree.person'),
        ),
    ]
