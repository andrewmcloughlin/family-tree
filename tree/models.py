from django.db import models
from datetime import date


# Create your models here.
class Person(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=120)
    middle_name = models.CharField(max_length=120,blank = True)
    last_name = models.CharField(max_length=120)
    def get_full_name(self): 
        if self.middle_name == "":
            return self.first_name + " " + self.last_name
        else:
            return self.first_name + " " + self.middle_name + " " + self.last_name
    GENDERS = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('X', 'Other'),
        ('U', 'Unknown'),
    )
    gender = models.CharField(max_length=1, choices=GENDERS)
    mother = models.ForeignKey('self', blank=True, null=True, default="", related_name='mother1', on_delete=models.CASCADE)
    father = models.ForeignKey('self', blank=True, null=True, default="", related_name='father1', on_delete=models.CASCADE)

    date_of_birth = models.DateField(null=True)
    date_of_death = models.DateField(null=True)
    is_alive = models.BooleanField()
    

    def __str__(self):
        return self.get_full_name()   

    def age(self):
        today = date.today()
        return today.year - self.date_of_birth.year - ((today.month, today.day) < (self.date_of_birth.month, self.date_of_birth.day))

class Union(models.Model):
    id = models.AutoField(primary_key=True)
    person1 = models.ForeignKey(Person, related_name='wife', on_delete=models.CASCADE, default=None)
    person2 = models.ForeignKey(Person, related_name='husband', on_delete=models.CASCADE, default=None)
    is_marriage = models.BooleanField()
