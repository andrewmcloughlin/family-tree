from django.forms import ModelForm
from .models import Person, Union

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'

class UnionForm(ModelForm):
    class Meta:
        model = Union
        fields = '__all__'