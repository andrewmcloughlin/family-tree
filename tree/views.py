from django.shortcuts import render

from .models import Person
from .forms import PersonForm, UnionForm

# Create your views here.

#CRUD -Create Retreive Update and Delete

# List all people
def person_list_view(request):
    person_objects = Person.objects.all()
    context = {
        'person_objects': person_objects
    }
    return render(request, "person/index.html", context)


def person_form(request):
    form = PersonForm()
    if request.method == "POST":
        form = PersonForm(request.POST)
        if form.is_valid():
            form.save()
            form = PersonForm()
    context = {
        'form': form
    }
    return render(request, 'person/add.html', context)


def union_form(request):
    form = UnionForm()
    if request.method == "POST":
        form = UnionForm(request.POST)
        if form.is_valid():
            form.save()
            form = UnionForm()
    context = {
        'form': form
    }
    return render(request, 'union/add-union.html', context)